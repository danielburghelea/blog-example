<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ImportBlogPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:posts {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command import the blog posts from the previous site';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $user = User::find($this->argument('user'));
            if ($user) {
                $url = 'https://candidate-test.sq1.io/api.php';

                $response = file_get_contents($url);
                $blogPostData = json_decode($response, true);

                if ($blogPostData['status'] == "ok" && $blogPostData['count'] > 0) {
                    foreach ($blogPostData['articles'] as $article) {
                        $post = new Post;
                        $post->title = $article['title'];
                        $post->description = $article['description'];
                        $post->publishedAt = Carbon::parse($article['publishedAt']);
                        $post->user_id = $user->id;
                        $post->save();
                    }
                }
            } else {
                return Command::FAILURE;
            }
        } catch (\Throwable $th) {
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}