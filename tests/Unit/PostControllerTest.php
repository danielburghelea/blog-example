<?php

namespace Tests\Unit;

use App\Http\Controllers\PostController;
use App\Models\Post;
use Illuminate\Http\Request;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function test_posts_are_returned_from_home()
    {
        $originalPosts = Post::factory()->count(10)->create();
        $request = new Request();
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');

        $parsedPosts = (new PostController)->loadMorePosts($request);
        $decodedContent = json_decode($parsedPosts->getContent());

        $this->assertEquals(200, $parsedPosts->getStatusCode());
        foreach ($originalPosts as $post) {
            $this->assertStringContainsStringIgnoringCase($post->title, $decodedContent->html);
        }
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function test_no_posts_are_returned_if_request_is_not_ajax()
    {
        $request = new Request();
        $parsedPosts = (new PostController)->loadMorePosts($request);
        $decodedContent = json_decode($parsedPosts->getContent());

        $this->assertEquals(200, $parsedPosts->getStatusCode());
        $this->assertEmpty($decodedContent->html);
    }
}
