<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    public function test_registration_screen_can_be_rendered()
    {
        $response = $this->get(route('register-user'));
        $response->assertStatus(200);
    }

    public function test_new_users_can_register()
    {
        $response = $this->post(route('register.custom'), [
            'name' => 'Test User',
            'email' => 'test@example.com',
            'password' => 'password'
        ]);
        $response->assertRedirect(route('dashboard'));
    }
}
