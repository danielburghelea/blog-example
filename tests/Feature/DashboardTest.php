<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test dashboard is visible for logged in user
     *
     * @return void
     */
    public function test_dashboard_for_logged_user(): void
    {
        $this->loginWithFakeUser();

        $response = $this->get(route('dashboard'));

        $response->assertStatus(200);
        $response->assertSee('My Posts');
        $response->assertSee('ID');
        $response->assertSee('Title');
        $response->assertSee('Description');
        $response->assertSee('Published At');
    }

    /**
     * Test dashboard is not visible for guest and gets a 302
     *
     * @return void
     */
    public function test_dashboard_for_guest(): void
    {
        $response = $this->get(route('dashboard'));
        $response->assertStatus(302);
    }
}
