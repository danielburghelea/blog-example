<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    use RefreshDatabase;

    public function test_new_post_screen_can_be_rendered()
    {
        $this->loginWithFakeUser();

        $response = $this->get(route('post.create'));
        $response->assertStatus(200);
    }

    public function test_new_post_is_created()
    {
        $this->loginWithFakeUser();

        $response = $this->post(route('post.store'), [
            'title' => 'Test Post',
            'description' => 'Lorem ipsum Lorem ipsum',
        ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('dashboard'));

        $this->followRedirects($response)->assertSee('Test Post');
        $this->followRedirects($response)->assertSee('Lorem ipsum Lorem ipsum');
    }

    /**
     * Test Create Post is not visible for guest and gets a 302
     *
     * @return void
     */
    public function test_create_post_is_not_visible_for_guest(): void
    {
        $response = $this->get(route('post.create'));
        $response->assertStatus(302);
    }
}
