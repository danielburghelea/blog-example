<nav
    class="md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden shadow-xl bg-white flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 px-6"
>
    <div
        class="md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto"
    >
        <button
            class="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
            type="button"
            onclick="toggleNavbar('example-collapse-sidebar')"
        >
            <i class="fas fa-bars"></i></button
        >
        <a
            class="md:block text-left md:pb-2 text-blueGray-600 mr-0 inline-block whitespace-nowrap text-sm uppercase font-bold p-4 px-0"
            href="javascript:void(0)"
        >
            Admin Page
        </a>
        <div
            class="md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded hidden"
            id="example-collapse-sidebar"
        >
            <div
                class="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-blueGray-200"
            >
                <div class="flex flex-wrap">
                    <div class="w-6/12">
                        <a
                            class="md:block text-left md:pb-2 text-blueGray-600 mr-0 inline-block whitespace-nowrap text-sm uppercase font-bold p-4 px-0"
                            href="javascript:void(0)"
                        >
                            Admin Page
                        </a>
                    </div>
                    <div class="w-6/12 flex justify-end">
                        <button
                            type="button"
                            class="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                            onclick="toggleNavbar('example-collapse-sidebar')"
                        >
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
            </div>
            <ul class="md:flex-col md:min-w-full flex flex-col list-none">
                <li class="items-center">
                    <a
                        class="text-blueGray-700 hover:text-blueGray-500 text-xs uppercase py-3 font-bold block"
                        href="{{route('dashboard')}}"
                    >
                        <i class="fas fa-tv opacity-75 mr-2 text-sm"></i>
                        Dashboard
                    </a
                    >
                </li>
                <li class="items-center">
                    <a
                        class="text-blueGray-700 hover:text-blueGray-500 text-xs uppercase py-3 font-bold block"
                        href="{{route('post.create')}}"
                    >
                        <i class="fas fa-tv opacity-75 mr-2 text-sm"></i>
                        Create Post
                    </a
                    >
                </li>
                <li class="items-center">
                    <a
                        class="text-blueGray-700 hover:text-blueGray-500 text-xs uppercase py-3 font-bold block"
                        href="/"
                    >
                        <i class="fas fa-newspaper text-blueGray-400 mr-2 text-sm"></i>
                        Landing Page
                    </a
                    >
                </li>
            </ul>
        </div>
    </div>
</nav>
