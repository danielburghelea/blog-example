<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <link rel="shortcut icon" href="./assets/img/favicon.ico" />
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/gh/creativetimofficial/tailwind-starter-kit/compiled-tailwind.min.css" />

    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <title>Some Blog</title>
</head>

<body class="text-gray-800 antialiased">
    <nav class="top-0 absolute z-50 w-full flex flex-wrap items-center justify-between px-2 py-3 ">
        <div class="container px-4 mx-auto flex flex-wrap items-center justify-between">
            <ul class="flex flex-col lg:flex-row list-none lg:ml-auto">
                @guest
                    <li class="flex items-center">
                        <a class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                            href="{{ route('login') }}">
                            <i class="lg:text-gray-300 text-gray-500 fab fa-login text-lg leading-lg "></i>
                            <span class=" inline-block ml-2">Login</span>
                        </a>
                    </li>
                    <li class="flex items-center">
                        <a class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                            href="{{ route('register-user') }}">
                            <i class="lg:text-gray-300 text-gray-500 fab fa-register text-lg leading-lg "></i>
                            <span class=" inline-block ml-2">Register</span>
                        </a>
                    </li>
                @else
                    <li class="flex items-center">
                        <a class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                            href="{{ route('signout') }}">
                            <i class="lg:text-gray-300 text-gray-500 fab fa-logout text-lg leading-lg "></i>
                            <span class=" inline-block ml-2">Logout</span>
                        </a>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>

    @yield('content')

</body>
<script>
    function toggleNavbar(collapseID) {
        document.getElementById(collapseID).classList.toggle("hidden");
        document.getElementById(collapseID).classList.toggle("block");
    }
</script>

@yield('js')

</html>
