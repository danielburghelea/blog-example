@extends('layouts.template')


@section('content')
    <main class="profile-page">
        <section class="relative block" style="height: 500px;">
            <div class="absolute top-0 w-full h-full bg-center bg-cover"
                 style='background-image: url("https://images.unsplash.com/photo-1499336315816-097655dcfbda?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=2710&amp;q=80");'>
                <span id="blackOverlay" class="w-full h-full absolute opacity-50 bg-black">

            <h1 class="blog-title">Some Blog</h1>
                </span>
            </div>
            <div class="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
                 style="height: 70px;">
                <svg class="absolute bottom-0 overflow-hidden" xmlns="http://www.w3.org/2000/svg"
                     preserveAspectRatio="none"
                     version="1.1" viewBox="0 0 2560 100" x="0" y="0">
                    <polygon class="text-gray-300 fill-current" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </section>

        <div class="col-md-12" id="post-data">
            @include('partials.posts')
        </div>


        <div class="ajax-load text-center" style="display:block">
            <p>
                <img class='loader-m-auto' src="/assets/img/loader.gif">
                Loading More post
            </p>
        </div>

    </main>
@endsection


@section('js')
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>
    <script type="text/javascript">
        let page = 1;

        $(document).ready(function () {
            loadMoreData(page);
            page++;
        });

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                loadMoreData(page);
                page++;
            }
        });

        function loadMoreData(page) {
            $.ajax({
                url: '/loadMorePosts/?page=' + page,
                type: "get",
                beforeSend: function () {
                    $('.ajax-load').show();
                }
            })
                .done(function (data) {
                    if (data.html === "") {
                        $('.ajax-load').html("No more records found");
                        return;
                    }
                    $('.ajax-load').hide();
                    $("#post-data").append(data.html);
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    alert('server not responding...');
                });

        }
    </script>
@endsection
